package Model;

import Controller.ServerHandler;
import View.TUI;

import java.util.ArrayList;

public class ComputerPlayer extends Player{
    public ComputerPlayer(String playerName,ArrayList<Card> hand,ServerHandler serverHandler){
        super(playerName, serverHandler);
    }


    /**
     * Plays the first allowed card in the hand, or the first card if no cards are allowed
     * @param topCard the card that's the highest on the discardPile
     * @return the card that the computer wants to play
     */
    @Override
    public Card doMovePlayer(Card topCard){
        for(Card card:this.getHand()){
            if(topCard.allowedMove(card, this,topCard)){
                return card;
            }
        }
       return this.getHand().get(0);
    }

    /**
     * Sets the color of the wildcard to red
     * @param card the card that you want the set the color of
     */
    @Override
    public void setColorWildcard(Card card){
        card.setColor(Card.Color.R);
    }

    /**
     * Returns yes when asked if the computer wants to play the drawn card.
     * @return yes
     */
    @Override
    public String playDrawnCard(){
        return "yes";
    }


}
