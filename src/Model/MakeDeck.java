package Model;

import java.util.ArrayList;
import java.util.Collections;

public class MakeDeck{
    public ArrayList<Card> cards;


    /**
     * Adds all the cards of the game to the Arraylist of cards.
     */
    public  MakeDeck(){
        cards = new ArrayList<>();
        cards.add(new Card(Card.Color.B,Card.Symbol.ZERO));
        cards.add(new Card(Card.Color.R,Card.Symbol.ZERO));
        cards.add(new Card(Card.Color.G,Card.Symbol.ZERO));
        cards.add(new Card(Card.Color.Y,Card.Symbol.ZERO));
        for(int i=0;i<2;i++){
            cards.add(new Card(Card.Color.B,Card.Symbol.ONE));
            cards.add(new Card(Card.Color.R,Card.Symbol.ONE));
            cards.add(new Card(Card.Color.G,Card.Symbol.ONE));
            cards.add(new Card(Card.Color.Y,Card.Symbol.ONE));
            cards.add(new Card(Card.Color.B,Card.Symbol.TWO));
            cards.add(new Card(Card.Color.R,Card.Symbol.TWO));
            cards.add(new Card(Card.Color.G,Card.Symbol.TWO));
            cards.add(new Card(Card.Color.Y,Card.Symbol.TWO));
            cards.add(new Card(Card.Color.B,Card.Symbol.THREE));
            cards.add(new Card(Card.Color.R,Card.Symbol.THREE));
            cards.add(new Card(Card.Color.G,Card.Symbol.THREE));
            cards.add(new Card(Card.Color.Y,Card.Symbol.THREE));
            cards.add(new Card(Card.Color.B,Card.Symbol.FOUR));
            cards.add(new Card(Card.Color.R,Card.Symbol.FOUR));
            cards.add(new Card(Card.Color.G,Card.Symbol.FOUR));
            cards.add(new Card(Card.Color.Y,Card.Symbol.FOUR));
            cards.add(new Card(Card.Color.B,Card.Symbol.FIVE));
            cards.add(new Card(Card.Color.R,Card.Symbol.FIVE));
            cards.add(new Card(Card.Color.G,Card.Symbol.FIVE));
            cards.add(new Card(Card.Color.Y,Card.Symbol.FIVE));
            cards.add(new Card(Card.Color.B,Card.Symbol.SIX));
            cards.add(new Card(Card.Color.R,Card.Symbol.SIX));
            cards.add(new Card(Card.Color.G,Card.Symbol.SIX));
            cards.add(new Card(Card.Color.Y,Card.Symbol.SIX));
            cards.add(new Card(Card.Color.B,Card.Symbol.SEVEN));
            cards.add(new Card(Card.Color.R,Card.Symbol.SEVEN));
            cards.add(new Card(Card.Color.G,Card.Symbol.SEVEN));
            cards.add(new Card(Card.Color.Y,Card.Symbol.SEVEN));
            cards.add(new Card(Card.Color.B,Card.Symbol.EIGHT));
            cards.add(new Card(Card.Color.R,Card.Symbol.EIGHT));
            cards.add(new Card(Card.Color.G,Card.Symbol.EIGHT));
            cards.add(new Card(Card.Color.Y,Card.Symbol.EIGHT));
            cards.add(new Card(Card.Color.B,Card.Symbol.NINE));
            cards.add(new Card(Card.Color.R,Card.Symbol.NINE));
            cards.add(new Card(Card.Color.G,Card.Symbol.NINE));
            cards.add(new Card(Card.Color.Y,Card.Symbol.NINE));
        }
        for(int i=0;i<2;i++){
            cards.add(new Card(Card.Color.B,Card.Symbol.R));
            cards.add(new Card(Card.Color.R,Card.Symbol.R));
            cards.add(new Card(Card.Color.G,Card.Symbol.R));
            cards.add(new Card(Card.Color.Y,Card.Symbol.R));
            cards.add(new Card(Card.Color.B,Card.Symbol.S));
            cards.add(new Card(Card.Color.R,Card.Symbol.S));
            cards.add(new Card(Card.Color.G,Card.Symbol.S));
            cards.add(new Card(Card.Color.Y,Card.Symbol.S));
            cards.add(new Card(Card.Color.B,Card.Symbol.D));
            cards.add(new Card(Card.Color.R,Card.Symbol.D));
            cards.add(new Card(Card.Color.G,Card.Symbol.D));
            cards.add(new Card(Card.Color.Y,Card.Symbol.D));
        }
        for(int i=0;i<4;i++){
            cards.add(new Card(Card.Color.W,Card.Symbol.W));
            cards.add(new Card(Card.Color.W,Card.Symbol.F));
        }
        Collections.shuffle(cards);
    }
}
